import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';
import { EventData } from 'express-sse-middleware';
import { MessageData, SseService } from './sse.service';

@Injectable()
export class SseMiddleware implements NestMiddleware {
  idCounter = 0;
  clientId = 0;
  clients = new Map<number, any>();

  constructor(private readonly sseService: SseService) {
    this.sseService.sseMsg$.subscribe((msgData: MessageData) => {
      [...this.clients.values()].forEach(sse => {
        this.idCounter += 1;
        const eventData: EventData<any> = {
          id: String(this.idCounter),
          event: msgData.event,
          data: msgData.data
        };
        console.log(eventData);
        sse.send(eventData);
      });
    });
  }

  use(req: Request, res: Response, next: () => void) {
    const sse = res.sse();
    this.clientId += 1;
    const clientId = this.clientId;
    this.clients.set(clientId, sse);
    req.on('close', () => {
      sse.close();
      this.clients.delete(clientId);
    });

    next();
  }
}
