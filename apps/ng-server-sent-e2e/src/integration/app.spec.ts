import { getGreeting } from '../support/app.po';

describe('ng-server-sent', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to ng-server-sent!');
  });
});
