import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ng-server-sent-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  eventSource = new EventSource('/api/status');
  status = { class: '', value: 'None' };
  isLogOpened = true;

  logs: any[] = [];

  ngOnInit() {
    this.eventSource.addEventListener(
      'log',
      (snapshot: any) => {
        let log = snapshot.data;
        console.log(log);
        const splitLog = log.split(',');
        if (splitLog.length === 1) {
          log = {
            output: `${splitLog[0]}`
          };
        } else if (splitLog.length === 2) {
          log = {
            isCommand: true,
            output: `<span class="code-log-status">${splitLog[0]}</span><span>${splitLog[1]}</span>`
          };
        }
        this.logs.push(log);
      },
      false
    );
    this.eventSource.addEventListener(
      'status',
      (snapshot: any) => {
        this.status = JSON.parse(snapshot.data);
      },
      false
    );
    this.eventSource.onmessage = (event: MessageEvent) => {
      if (event.data === 'stop') {
        this.eventSource.close();
      }
    };
  }

  openLogDetails() {
    this.isLogOpened = !this.isLogOpened;
  }
}
