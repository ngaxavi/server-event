import { Injectable, NotFoundException } from '@nestjs/common';
import { Message, Log } from '@ng-server-sent/api-interfaces';
import { InjectModel } from 'nestjs-typegoose';
import { ReturnModelType } from '@typegoose/typegoose';

@Injectable()
export class AppService {
  private logs: string[] = [
    `Fetching changes started.`,
    `Fetching changes finished.`,
    `Execute,docker build --cpu-period=100000 --cpu-quota=200000 -m 3072m --force-rm=true -t 148951_243854:1 --pull -f "Dockerfile" .`,
    `Sending build context to Docker daemon  61.44kB`,
    `Step 1/2 : FROM node:8`,
    `8: Pulling from library/node`,
    `d660b1f15b9b: Pulling fs layer`,
    `46dde23c37b3: Pulling fs layer`,
    `6ebaeb074589: Pulling fs layer`,
    `e7428f935583: Pulling fs layer`,
    `eda527043444: Pulling fs layer`,
    `f3088daa8887: Pulling fs layer`,
    `5b9236fe759e: Pulling fs layer`,
    `bd3513780305: Pulling fs layer`,
    `f3088daa8887: Waiting`,
    `5b9236fe759e: Waiting`,
    `bd3513780305: Waiting`,
    `e7428f935583: Waiting`,
    `46dde23c37b3: Verifying Checksum`,
    `46dde23c37b3: Download complete`,
    `6ebaeb074589: Verifying Checksum`,
    `6ebaeb074589: Download complete`,
    `d660b1f15b9b: Verifying Checksum`,
    `d660b1f15b9b: Download complete`,
    `eda527043444: Download complete`,
    `f3088daa8887: Verifying Checksum`,
    `f3088daa8887: Download complete`,
    `bd3513780305: Verifying Checksum`,
    `bd3513780305: Download complete`,
    `5b9236fe759e: Verifying Checksum`,
    `5b9236fe759e: Download complete`,
    `e7428f935583: Verifying Checksum`,
    `e7428f935583: Download complete`,
    `d660b1f15b9b: Pull complete`,
    `46dde23c37b3: Pull complete`,
    `6ebaeb074589: Pull complete`,
    `e7428f935583: Pull complete`,
    `eda527043444: Pull complete`,
    `f3088daa8887: Pull complete`,
    `5b9236fe759e: Pull complete`,
    `bd3513780305: Pull complete`,
    `Digest: sha256:cd8ebd022c01f519eb58a98fcbb05c1d1195ac356ef01851036671ec9e9d5580`,
    `Status: Downloaded newer image for node:8`,
    ` ---&gt; 55791187f71c`,
    `Step 2/2 : CMD node -v`,
    ` ---&gt; Running in 1f7f3f7fec17`,
    `Removing intermediate container 1f7f3f7fec17`,
    ` ---&gt; e611823719e7`,
    `Successfully built e611823719e7`,
    `Successfully tagged 148951_243854:1`,
    `Execute,docker image prune -f`,
    `Total reclaimed space: 0B`,
    `Execute,docker images`,
    `REPOSITORY          TAG                 IMAGE ID            CREATED                  SIZE`,
    `148951_243854       1                   e611823719e7        Less than a second ago   673MB`,
    `node                8                   55791187f71c        6 days ago               673MB`,
    `Success,Build finished successfully!.`
  ];

  getData(): Message {
    return { message: 'Welcome to api!' };
  }

  getLogs(): string[] {
    return this.logs;
  }
}
