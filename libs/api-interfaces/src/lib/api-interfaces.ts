export interface Message {
  message: string;
}

export interface Log {
  class: string;
  output: string;
  status?: string;
}
