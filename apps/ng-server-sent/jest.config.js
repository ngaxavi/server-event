module.exports = {
  name: 'ng-server-sent',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/ng-server-sent',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
