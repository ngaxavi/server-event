import { Controller, Get, Res } from '@nestjs/common';
import { AppService } from './app.service';
import { SseService } from './sse';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly sseService: SseService
  ) {}

  @Get('status')
  getStatus(@Res() res) {
    const logs = this.appService.getLogs();
    let i = 0;
    this.sseService.fire('status', {
      class: 'running',
      value: 'Running'
    });
    const inter = setInterval(() => {
      if (i <= logs.length - 1) {
        console.log(logs[i]);
        this.sseService.fire('log', logs[i]);
      } else if (i === logs.length) {
        this.sseService.fire('status', {
          class: 'successful',
          value: 'Completed'
        });
      } else {
        clearInterval(inter);
        this.sseService.fire('message', 'stop');
        res.end();
      }
      i++;
    }, 1000);

    return res;
  }
}
