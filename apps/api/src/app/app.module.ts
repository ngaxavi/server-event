import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { SseMiddleware, SseService } from './sse';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  controllers: [AppController],
  providers: [AppService, SseService]
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(SseMiddleware).forRoutes(AppController);
  }
}
