import { Injectable } from '@nestjs/common';
import { BehaviorSubject, Observable } from 'rxjs';

export interface MessageData {
  event: string;
  data: any;
}

@Injectable()
export class SseService {
  private sseMsg = new BehaviorSubject<MessageData>({
    event: 'init',
    data: 'initial msg'
  });
  public sseMsg$: Observable<MessageData> = this.sseMsg.asObservable();

  fire(event: string, data: any) {
    this.sseMsg.next({ event, data });
  }
}
